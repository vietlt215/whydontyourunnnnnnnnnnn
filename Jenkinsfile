pipeline {
    agent {
        label "kube"
    }

    environment {
        ARTIFACT_REPOSITORY = "acracr2.azurecr.io"
        CREDENTIAL_ID = "acracr2"
        BUILD_IMAGE_BE = "acracr2.azurecr.io/be:${GIT_COMMIT}"
        BUILD_IMAGE_FE = "acracr2.azurecr.io/fe:${GIT_COMMIT}"
    }

    stages {
        stage ("Validate") {
            steps {
                sh "docker version"
            }
        }

        stage ("Build docker image BE nodejs") {
            steps {
                sh "docker build -t ${BUILD_IMAGE_BE} ."
                sh "docker build -t ${BUILD_IMAGE_FE} ."
            }
        }

        stage ("Push docker image to ACR") {
            steps {
                withDockerRegistry([credentialsId: "${CREDENTIAL_ID}", url: "http://${ARTIFACT_REPOSITORY}"]) {
                    sh "docker push ${BUILD_IMAGE_BE}"
                    sh "docker push ${BUILD_IMAGE_FE}"
                }
            }
        }

        stage ("Deploy new images to AKS") {
            steps {
                withCredentials([file(credentialsId: 'kubeconfig', variable: 'KUBECRED')]) {
                    sh "cat $KUBECRED > ~/.kube/config"
                    sh "kubectl set image deployment/nodejs-backend nodejs-backend=${BUILD_IMAGE_BE}"
                    sh "kubectl set image deployment/angular-frontend angular-frontend=${BUILD_IMAGE_FE}"
                }
            }
        }
    }

    post {
        always {
            echo 'Cleaning up workspace'
            sh "docker rmi ${BUILD_IMAGE_BE} ${BUILD_IMAGE_FE} || true"
            deleteDir() 
        }
    }
}
