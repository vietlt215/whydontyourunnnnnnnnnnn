const Pool = require('pg').Pool
const pool = new Pool({
  user: process.env.DB_USER || 'auser@thisisapsqlserver1423576',
  host: process.env.DB_HOST || 'thisisapsqlserver1423576.postgres.database.azure.com',
  database: process.env.DB_NAME || 'userdb',
  password: process.env.DB_PASSWORD || 'Adm!n123',
  port: 5432,
  ssl: true
})

const getUsers = (request, response) => {
  
  pool.query('SELECT * FROM users', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

module.exports = {
  getUsers
}
